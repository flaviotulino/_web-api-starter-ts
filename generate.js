///<reference path="typings/node/node.d.ts"/>
///<reference path="typings/sequelize/sequelize.d.ts"/>
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var Sequelize = require('sequelize');
var db = new Sequelize('mysql://root:root@localhost:3306/CNDM');
var fs = require('fs');
(function test() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            if (fs.existsSync("database/DatabaseTest.ts")) {
                fs.unlinkSync("database/DatabaseTest.ts");
            }
            fs.appendFileSync("database/DatabaseTest.ts", '\/' + '\/' + '\/<' + '<reference path="../typings/node/node.d.ts"/>///<reference path="../typings/sequelize/sequelize.d.ts"');
            fs.appendFileSync("database/DatabaseTest.ts", `
    var Sequelize = require('sequelize');
    var db = new Sequelize('mysql://root:root@localhost:3306/CNDM');
    export class Database {
    `);
            var tables = yield db.query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'CNDM'", { type: Sequelize.QueryTypes.SELECT });
            for (var t of tables) {
                if (fs.existsSync(`database/models/${t.TABLE_NAME}Test.ts`)) {
                    fs.unlinkSync(`database/models/${t.TABLE_NAME}Test.ts`);
                }
                var tableNameSingular = t.TABLE_NAME;
                // IS PLURAL
                if (t.TABLE_NAME[t.TABLE_NAME.length - 1] == "s") {
                    tableNameSingular = t.TABLE_NAME.slice(0, -1);
                }
                fs.appendFileSync(`database/models/${tableNameSingular}Test.ts`, `export class ${tableNameSingular} {`);
                fs.appendFileSync("database/DatabaseTest.ts", `public ${t.TABLE_NAME} = db.define('${t.TABLE_NAME}', {`);
                var columns = yield db.query(`SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'CNDM' AND TABLE_NAME = '${t.TABLE_NAME}'`, { type: Sequelize.QueryTypes.SELECT });
                var i = 1;
                var length = columns.length;
                for (var c of columns) {
                    var field = c.COLUMN_NAME;
                    var type;
                    var classType;
                    switch (c.DATA_TYPE) {
                        case 'int':
                            type = "INTEGER";
                            classType = "number";
                            break;
                        case 'varchar':
                            type = "STRING";
                            classType = "string";
                    }
                    if (c.COLUMN_NAME == 'id') {
                        i++;
                        fs.appendFileSync(`database/models/${tableNameSingular}Test.ts`, `${field} : ${classType};`);
                        continue;
                    }
                    //TABLES[`${t.TABLE_NAME}`][`${c.COLUMN_NAME}`] = `${c.DATA_TYPE}`;
                    if (i < length && c.COLUMN_NAME != 'id') {
                        fs.appendFileSync("database/DatabaseTest.ts", `${field} : Sequelize.${type} ,`);
                        fs.appendFileSync(`database/models/${tableNameSingular}Test.ts`, `${field} : ${classType};`);
                    }
                    else {
                        fs.appendFileSync(`database/models/${tableNameSingular}Test.ts`, "}");
                        fs.appendFileSync("database/DatabaseTest.ts", `${field} : Sequelize.${type} }, {timestamp : false});`);
                    }
                    i++;
                }
            }
            fs.appendFileSync("database/DatabaseTest.ts", "}");
        }
        catch (ex) {
            console.error(ex);
        }
    });
}());
/*

 db.query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'CNDM'")
 .spread(function (res, meta) {

 console.log(res);

 for (var table of res) {
 TABLES[`${table.TABLE_NAME}`] = {};
 a();
 }
 });


 function a() {
 for (var table of TABLES) {
 db.query(`SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'CNDM' AND TABLE_NAME = '${table.TABLE_NAME}'`)
 .spread(function (res, meta) {
 for (var col of res) {
 TABLES[`${table.TABLE_NAME}`][`${col.COLUMN_NAME}`] = `${col.DATA_TYPE}`;
 console.log(TABLES);
 }
 });
 }
 }
 */
