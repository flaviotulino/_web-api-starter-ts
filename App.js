///<reference path="typings/node/node.d.ts"/>
///<reference path="typings/express/express.d.ts"/>
///<reference path="./middleware/Authentication.ts" />
"use strict";
class App {
    run() {
        var express = require('express');
        var app = express();
        var path = require('path');
        require('./database/sync');
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'jade');
        app.use(express.static(path.join(__dirname, 'public')));
        app.use("/lib", express.static(path.join(__dirname, 'public/bower_components')));
        var bodyParser = require('body-parser');
        app.use(bodyParser.json()); // support json encoded bodies
        app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
        // API routes
        app.use('/api', require('./routes/api'));
        /*// Shared variables
        app.use(function (req, res, next) {
            res.locals.scripts = fs.readdirSync('./public/javascripts');
            next();
        });*/
        // Web routes
        app.use('/', require('./routes/web'));
        // Error handler
        app.use(function (req, res, next) {
            // the status option, or res.statusCode = 404
            // are equivalent, however with the option we
            // get th e "status" local available as well
            return res.status(404).json({
                message: "The resource you was looking for does not exists"
            });
        });
        app.listen(4000, function () {
            console.log('Example app listening on port 3000!');
        });
    }
}
exports.App = App;
module.exports = App;
