import {Application} from "./Application";
import DateTimeFormat = Intl.DateTimeFormat;
export class Account{
    id:number;
    email:string;
    password:string;
    createdAt:Date;
    updatedAt:Date;
    Applications:Application[];
}
