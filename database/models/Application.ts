import DateTimeFormat = Intl.DateTimeFormat;
import {Account} from "./Account";
export class Application {
    id:number;
    name:string;
    createdAt:Date;
    updatedAt:Date;
    Owner:Account;
}
