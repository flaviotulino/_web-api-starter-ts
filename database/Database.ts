///<reference path="../typings/node/node.d.ts"/>
///<reference path="../typings/sequelize/sequelize.d.ts"/>
var Sequelize = require('sequelize');
var db = new Sequelize('mysql://root@localhost/Condominium');

export class Database {
    public Account = db.define('account', {
        id: {
            autoIncrement: true,
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        email: Sequelize.STRING,
        password: Sequelize.STRING,
    });

    public Application = db.define('application', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: Sequelize.STRING
    });

    constructor() {
        this.Account.hasMany(this.Application);
        this.Application.belongsTo(this.Account);
    }
}