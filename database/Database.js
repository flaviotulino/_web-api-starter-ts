"use strict";
///<reference path="../typings/node/node.d.ts"/>
///<reference path="../typings/sequelize/sequelize.d.ts"/>
var Sequelize = require('sequelize');
var db = new Sequelize('mysql://root@localhost/Condominium');
class Database {
    constructor() {
        this.Account = db.define('account', {
            id: {
                autoIncrement: true,
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            email: Sequelize.STRING,
            password: Sequelize.STRING,
        });
        this.Application = db.define('application', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: Sequelize.STRING
        });
        this.Account.hasMany(this.Application);
        this.Application.belongsTo(this.Account);
    }
}
exports.Database = Database;
