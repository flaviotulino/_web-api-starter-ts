import {Response, Request} from "express";
import {Database} from "../database/Database";
var CryptoJS = require("crypto-js");

export class Authentication {
    public static User;

    public static Authorized(request, response:Response, next) {
        if (request.headers.authorization == null) {
            return response.status(401).json({message: "Unauthorized"});
        }

        try {
            //let tokenEncrypted = CryptoJS.AES.decrypt(request.headers.authorization.toString(), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf');

            var bytes = CryptoJS.AES.decrypt(request.headers.authorization.toString(), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf');
            var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

            console.log(new Date(decryptedData.expiresAt) < new Date());
            
            if (new Date(decryptedData.expiresAt) < new Date()) {
                return response.status(401).json({message: "Token Expired"});
            }

            let db = new Database();
            db.Account.findOne({
                where: {
                    id: decryptedData.id
                }
            }).then((user) => {
                console.log(user);
                Authentication.User = user;
                next();
            });
        } catch (e) {
            console.log(e);
            return response.status(401).json({message: "Unauthorized"});
        }


        //let decodeToken
        /*else {
         Authentication.User = 1;
         next();
         }*/
    }
}

/*
 // Encrypt
 var ciphertext = CryptoJS.AES.encrypt('my message', 'secret key 123');

 // Decrypt
 var bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'secret key 123');
 var plaintext = bytes.toString(CryptoJS.enc.Utf8);*/
