"use strict";
const Database_1 = require("../database/Database");
var CryptoJS = require("crypto-js");
class Authentication {
    static Authorized(request, response, next) {
        if (request.headers.authorization == null) {
            return response.status(401).json({ message: "Unauthorized" });
        }
        try {
            //let tokenEncrypted = CryptoJS.AES.decrypt(request.headers.authorization.toString(), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf');
            var bytes = CryptoJS.AES.decrypt(request.headers.authorization.toString(), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf');
            var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            console.log(new Date(decryptedData.expiresAt) < new Date());
            if (new Date(decryptedData.expiresAt) < new Date()) {
                return response.status(401).json({ message: "Token Expired" });
            }
            let db = new Database_1.Database();
            db.Account.findOne({
                where: {
                    id: decryptedData.id
                }
            }).then((user) => {
                console.log(user);
                Authentication.User = user;
                next();
            });
        }
        catch (e) {
            console.log(e);
            return response.status(401).json({ message: "Unauthorized" });
        }
        //let decodeToken
        /*else {
         Authentication.User = 1;
         next();
         }*/
    }
}
exports.Authentication = Authentication;
/*
 // Encrypt
 var ciphertext = CryptoJS.AES.encrypt('my message', 'secret key 123');

 // Decrypt
 var bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'secret key 123');
 var plaintext = bytes.toString(CryptoJS.enc.Utf8);*/
