import {BaseController} from "./BaseController";
import {Application} from "../database/models/Application";
import isEmpty = require("lodash/isEmpty");
export class ApplicationController extends BaseController {
    public async add() {
        let application = new Application();
        application.name = this.params.name;
        application.Owner = this.AuthUser;

        let newApplication = await this.database.Application.create(application);
        console.log(newApplication);

        if (isEmpty(newApplication)) {
            return this.response.status(500).json({
                message: "Unable to create the application"
            })
        }

        return newApplication;
    }
}

