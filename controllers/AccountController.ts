import {BaseController} from "./BaseController";
import {Account} from "../database/models/Account";
import isEmpty = require("lodash/isEmpty");
var CryptoJS = require("crypto-js");
var md5 = require('md5');
var Enumerable = require('../linq.min');

export class AccountController extends BaseController {
    public async registration() {
        let account = new Account();
        account.email = this.params.email;
        account.password = md5(this.params.password);

        await this.database.Account.create(account);

        return account;

    }

    public async login() {
        let account = new Account();
        account.email = this.params.email;
        account.password = md5(this.params.password);

        let user:Account = await this.database.Account.findOne({
            where: {
                email: account.email,
                password: account.password
            }
        });


        if (isEmpty(user)) {
            this.response.status(404);
            return {
                message: "Unable to find this user",
                access_token: null
            };
        } else {
            let timestamp = new Date().toISOString();
            /*await this.database.Account.update({
             lastLogin: timestamp
             },
             {
             where: {
             id: user.id
             }
             });*/


            let date = new Date(timestamp);
            date.setDate(date.getDate() + 30);

            let tokenResponse = {
                id: user.id,
                email: user.email,
                lastLogin: timestamp,
                expiresAt: date.toISOString()
            };


            let token = CryptoJS.AES.encrypt(JSON.stringify(tokenResponse), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf').toString();

            return {
                message: null,
                access_token: token
            }
        }
    }


    public async applications() {
        var response = new Account();
        response.id = this.AuthUser.id;
        response.email = this.AuthUser.email;
        response.Applications = await this.database.Application.findAll({
            where: {
                accountId: this.AuthUser.id
            }
        });

        response.Applications = Enumerable.From(response.Applications).Select(function (a) {
            return {id: a.id, name: a.name, createdAt: a.createdAt}
        }).ToArray();
        console.log(response.Applications);

        return this.response.json(response);

    }

    public async info() {
        return await this.database.Account.find({
            include: [this.database.Application]
        });
    }
}