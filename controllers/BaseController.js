"use strict";
const Database_1 = require("../database/Database");
const Authentication_1 = require("../middleware/Authentication");
const isEmpty = require("lodash/isEmpty");
/**
 * @class
 */
class BaseController {
    constructor(request, response) {
        this.request = request;
        this.response = response;
        this.params = !isEmpty(request.query) ? request.query : request.body;
        this.database = new Database_1.Database();
        this.AuthUser = Authentication_1.Authentication.User;
    }
}
exports.BaseController = BaseController;
