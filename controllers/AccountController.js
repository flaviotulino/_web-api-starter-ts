"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const BaseController_1 = require("./BaseController");
const Account_1 = require("../database/models/Account");
const isEmpty = require("lodash/isEmpty");
var CryptoJS = require("crypto-js");
var md5 = require('md5');
var Enumerable = require('../linq.min');
class AccountController extends BaseController_1.BaseController {
    registration() {
        return __awaiter(this, void 0, void 0, function* () {
            let account = new Account_1.Account();
            account.email = this.params.email;
            account.password = md5(this.params.password);
            yield this.database.Account.create(account);
            return account;
        });
    }
    login() {
        return __awaiter(this, void 0, void 0, function* () {
            let account = new Account_1.Account();
            account.email = this.params.email;
            account.password = md5(this.params.password);
            let user = yield this.database.Account.findOne({
                where: {
                    email: account.email,
                    password: account.password
                }
            });
            if (isEmpty(user)) {
                this.response.status(404);
                return {
                    message: "Unable to find this user",
                    access_token: null
                };
            }
            else {
                let timestamp = new Date().toISOString();
                /*await this.database.Account.update({
                 lastLogin: timestamp
                 },
                 {
                 where: {
                 id: user.id
                 }
                 });*/
                let date = new Date(timestamp);
                date.setDate(date.getDate() + 30);
                let tokenResponse = {
                    id: user.id,
                    email: user.email,
                    lastLogin: timestamp,
                    expiresAt: date.toISOString()
                };
                let token = CryptoJS.AES.encrypt(JSON.stringify(tokenResponse), 'dgdsggsfgfdfggfdfgdgfdgdfgehsbf').toString();
                return {
                    message: null,
                    access_token: token
                };
            }
        });
    }
    applications() {
        return __awaiter(this, void 0, void 0, function* () {
            var response = new Account_1.Account();
            response.id = this.AuthUser.id;
            response.email = this.AuthUser.email;
            response.Applications = yield this.database.Application.findAll({
                where: {
                    accountId: this.AuthUser.id
                }
            });
            response.Applications = Enumerable.From(response.Applications).Select(function (a) {
                return { id: a.id, name: a.name, createdAt: a.createdAt };
            }).ToArray();
            console.log(response.Applications);
            return this.response.json(response);
        });
    }
    info() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.database.Account.find({
                include: [this.database.Application]
            });
        });
    }
}
exports.AccountController = AccountController;
