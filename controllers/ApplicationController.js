"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const BaseController_1 = require("./BaseController");
const Application_1 = require("../database/models/Application");
const isEmpty = require("lodash/isEmpty");
class ApplicationController extends BaseController_1.BaseController {
    add() {
        return __awaiter(this, void 0, void 0, function* () {
            let application = new Application_1.Application();
            application.name = this.params.name;
            application.Owner = this.AuthUser;
            let newApplication = yield this.database.Application.create(application);
            console.log(newApplication);
            if (isEmpty(newApplication)) {
                return this.response.status(500).json({
                    message: "Unable to create the application"
                });
            }
            return newApplication;
        });
    }
}
exports.ApplicationController = ApplicationController;
