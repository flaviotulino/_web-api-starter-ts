///<reference path="../typings/express/express.d.ts"/>
import {Request, Response} from "express";
import {Database} from "../database/Database";
import {Authentication} from "../middleware/Authentication";
import isEmpty = require("lodash/isEmpty");
import {Account} from "../database/models/Account";

/**
 * @class
 */
export abstract class BaseController {
    /** @type Request contains the incoming request */
    protected request:Request;
    /** @type Response contains methods to perferm a response */
    protected response:Response;
    /** @type request params */
    protected params;
    /** @type Database a reference to the open database */
    protected database:Database;

    protected AuthUser:Account;

    constructor(request:Request, response:Response) {
        this.request = request;
        this.response = response;
        this.params = !isEmpty(request.query) ? request.query : request.body;
        this.database = new Database();
        this.AuthUser = Authentication.User;
    }
}


