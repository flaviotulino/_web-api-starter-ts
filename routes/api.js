"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const AccountController_1 = require("../controllers/AccountController");
const Authentication_1 = require("../middleware/Authentication");
var express = require('express');
var router = express.Router();
/** Public routes **/
router
    .put('/registration', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.json(yield new AccountController_1.AccountController(req, res).registration());
}))
    .post('/login', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.json(yield new AccountController_1.AccountController(req, res).login());
}))
    .get("/test", (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.json(yield new AccountController_1.AccountController(req, res).info());
}))
    .use((req, res, next) => {
    Authentication_1.Authentication.Authorized(req, res, next);
})
    .use("/account", require('./api/account'))
    .use("/application", require("./api/application"));
module.exports = router;
