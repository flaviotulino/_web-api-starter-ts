import {AccountController} from "../controllers/AccountController";
var express = require('express');
var router = express.Router();

/** Public routes **/
router
    .get('/login', (req, res) => {
        res.render('login', {});
    })
    .get('/', (req, res) => {
        res.render('dashboard/home', {});
    })
    .get("/test", async(req, res)=> {
        //let info = new AccountController(req, res).info();
        res.render('index', {Accounts: await new AccountController(req, res).info()})
    });


module.exports = router;