"use strict";
///<reference path="../../typings/node/node.d.ts"/>
const BirdsController_1 = require("../../controllers/BirdsController");
const Authentication_1 = require("../../middleware/Authentication");
var express = require('express');
var router = express.Router();
// middleware that is specific to this router
router.use((req, res, next) => {
    Authentication_1.Authentication.Authorized(req, res, next);
});
// define the home page route
router.get('/info', (req, res) => new BirdsController_1.BirdsController(req, res).info());
// define the about route
router.get('/about', function (req, res) {
    res.send('About birds');
});
module.exports = router;
