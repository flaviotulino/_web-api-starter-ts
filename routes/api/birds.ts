///<reference path="../../typings/node/node.d.ts"/>
import {BirdsController} from "../../controllers/BirdsController";
import {Authentication} from "../../middleware/Authentication";
var express = require('express');
var router = express.Router();

// middleware that is specific to this router
router.use((req, res, next) => {
    Authentication.Authorized(req, res, next);
});

// define the home page route
router.get('/info', (req, res) => new BirdsController(req, res).info());

// define the about route
router.get('/about', function (req, res) {
    res.send('About birds');
});

module.exports = router;
