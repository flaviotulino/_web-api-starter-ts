"use strict";
///<reference path="../../typings/node/node.d.ts"/>
const AccountController_1 = require("../../controllers/AccountController");
var express = require('express');
var router = express.Router();
router.get('/applications', (req, res) => new AccountController_1.AccountController(req, res).applications());
module.exports = router;
