///<reference path="../../typings/node/node.d.ts"/>
import {AccountController} from "../../controllers/AccountController";
var express = require('express');
var router = express.Router();

router.get('/applications', (req, res)=> new AccountController(req, res).applications());

module.exports = router;
