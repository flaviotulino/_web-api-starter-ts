import {ApplicationController} from "../../controllers/ApplicationController";
var express = require('express');
var router = express.Router();

router.put('/add', async (req, res)=> {
    res.json(await new ApplicationController(req, res).add());
});

module.exports = router;