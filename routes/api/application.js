"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ApplicationController_1 = require("../../controllers/ApplicationController");
var express = require('express');
var router = express.Router();
router.put('/add', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.json(yield new ApplicationController_1.ApplicationController(req, res).add());
}));
module.exports = router;
