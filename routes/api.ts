import {AccountController} from "../controllers/AccountController";
import {Authentication} from "../middleware/Authentication";
var express = require('express');
var router = express.Router();

/** Public routes **/
router
    .put('/registration', async(req, res) => {
        res.json(await new AccountController(req, res).registration());
    })
    .post('/login', async(req, res) => {
        res.json(await new AccountController(req, res).login())
    })

    .get("/test", async(req, res) => {
        res.json(await new AccountController(req, res).info());
    })


    /** Following routes require authorization header **/
    .use((req, res, next) => {
        Authentication.Authorized(req, res, next);
    })

    .use("/account", require('./api/account'))
    .use("/application", require("./api/application"));

module.exports = router;