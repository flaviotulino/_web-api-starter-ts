"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const AccountController_1 = require("../controllers/AccountController");
var express = require('express');
var router = express.Router();
/** Public routes **/
router
    .get('/login', (req, res) => {
    res.render('login', {});
})
    .get('/', (req, res) => {
    res.render('dashboard/home', {});
})
    .get("/test", (req, res) => __awaiter(this, void 0, void 0, function* () {
    //let info = new AccountController(req, res).info();
    res.render('index', { Accounts: yield new AccountController_1.AccountController(req, res).info() });
}));
module.exports = router;
